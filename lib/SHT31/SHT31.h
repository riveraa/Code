#ifndef SHT31_H
#define SHT31_H

#include "mbed.h"

#define SHT31_ADDR_LOW             0x44 << 1 // 7-bit address
#define SHT31_ADDR_HIGH            0x45 << 1 // 7-bit address
#define SHT31_MEAS_HIGHREP_STRETCH 0x2C06
#define SHT31_MEAS_MEDREP_STRETCH  0x2C0D
#define SHT31_MEAS_LOWREP_STRETCH  0x2C10
#define SHT31_MEAS_HIGHREP         0x2400
#define SHT31_MEAS_MEDREP          0x240B
#define SHT31_MEAS_LOWREP          0x2416
#define SHT31_READSTATUS           0xF32D
#define SHT31_CLEARSTATUS          0x3041
#define SHT31_SOFTRESET            0x30A2
#define SHT31_HEATEREN             0x306D
#define SHT31_HEATERDIS            0x3066

class SHT31
{
public:
	SHT31(PinName sda, PinName scl, bool fahrenheit, bool addrPinHigh = false);
	float readTemperature ();
	float readHumidity ();
	bool update (uint8_t tries = 15);

private:
	void reset ();
	uint16_t readStatus (uint8_t tries = 2);
	void writeCommand (uint16_t cmd);
	uint8_t checksumTest (char * data, int len, uint8_t crc8);

	I2C i2c;
	int addr;
	float humidity, temp;
	bool fahrenheit;
};

#endif // ifndef SHT31_H
