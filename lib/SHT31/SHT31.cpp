#include "SHT31.h"
#include "mbed.h"

SHT31::SHT31(PinName sda, PinName scl, bool fahrenheit, bool addrPinHigh) : i2c (sda, scl)
{
	addr = addrPinHigh ? SHT31_ADDR_HIGH : SHT31_ADDR_LOW;

	this->fahrenheit = fahrenheit; // True = Fahrenheit - False = Celsius

	reset();
	readStatus();
}

float SHT31::readTemperature (void)
{
	return temp;
}

float SHT31::readHumidity (void)
{
	return humidity;
}

void SHT31::reset ()
{
	writeCommand (SHT31_SOFTRESET);
	wait_ns (10000);
}

uint16_t SHT31::readStatus (uint8_t tries)
{
	char val[3];
	uint16_t stat;

	if (tries <= 0)
		return -1;

	writeCommand (SHT31_READSTATUS);

	i2c.read (addr, val, 3);

	stat = ((uint16_t) val[0]) << 8 | val[1];

	if (!checksumTest (val, 2, val[2]))
		return readStatus (tries - 1);

	return stat;
}

void SHT31::writeCommand (uint16_t cmd)
{
	char buf[2];

	buf[0] = (cmd >> 8);
	buf[1] = (cmd & 0xFF);

	i2c.write (addr, buf, 2);
}

bool SHT31::update (uint8_t tries)
{
	if (tries <= 0)
		return false;

	uint16_t ST, SH;
	char readbuffer[6];

	writeCommand (SHT31_MEAS_HIGHREP_STRETCH);

	i2c.read (addr, readbuffer, 6);

	ST = ((uint16_t) readbuffer[0]) << 8 | readbuffer[1];
	SH = ((uint16_t) readbuffer[3]) << 8 | readbuffer[4];

	if (!checksumTest (readbuffer + 3, 2, readbuffer[5]) || !checksumTest (readbuffer, 2, readbuffer[2]))
		return update (tries - 1);

	if (fahrenheit)
		temp = ((float) (((uint32_t) ST) * 315)) / 0xFFFF - 49;
	else
		temp = ((float) (((uint32_t) ST) * 175)) / 0xFFFF - 45;

	humidity = ((float) (((uint32_t) SH) * 100)) / 0xFFFF;

	return true;
}

uint8_t SHT31::checksumTest (char * data, int len, uint8_t crc8)
{
	const uint8_t POLYNOMIAL (0x31);
	uint8_t crc (0xFF);

	for (int j = len; j > 0; j--)
	{
		crc ^= *data++;

		for (int i = 8; i > 0; i--)
			crc = (crc & 0x80) ? (crc << 1) ^ POLYNOMIAL : (crc << 1);
	}

	return crc == crc8;
}
