#ifndef SENSORS_H
#define SENSORS_H

#include "SHT31.h"
#include "sht15.hpp"

class Sensors
{
public:
	Sensors();
	void initSensors ();
	void getSensorsIntValue (int * temp1, int * humi1, int * temp2, int * humi2, int * temp3, int * humi3);

	SHT31 sensor1;
	SHT31 sensor2;
	SHTx::SHT15 sensor3;
};

extern Sensors sensors;

#endif	// ifndef SENSORS_H