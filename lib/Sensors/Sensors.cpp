#include "Sensors.h"

Sensors::Sensors() :  sensor1 (I2C_SDA, I2C_SCL, false, false), sensor2 (I2C_SDA, I2C_SCL, false, true), sensor3 (D7, D6)
{ }

void Sensors::initSensors ()
{
	sensor3.setOTPReload (false);
	sensor3.setResolution (true);
	sensor3.setScale (false);
}

void Sensors::getSensorsIntValue (int * temp1, int * humi1, int * temp2, int * humi2, int * temp3, int * humi3)
{
	sensor1.update();
	sensor2.update();
	sensor3.update();

	*temp1 = (int) (sensor1.readTemperature() * 10.0);	// Temperature - Air 1
	*humi1 = (int) (sensor1.readHumidity());			// Humidity - Air 1

	*temp2 = (int) (sensor2.readTemperature() * 10.0);	// Temperature - Air 2
	*humi2 = (int) (sensor2.readHumidity());			// Humidity - Air 2

	*temp3 = (int) (sensor3.getTemperature() * 10.0);	// Temperature - Sol
	*humi3 = (int) (sensor3.getHumidity());				// Humidity - Sol
}

Sensors sensors;